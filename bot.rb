require 'telegram_bot'
token = '1232318751:AAENs5IXJWeMvHvlpwNUOipI3JFqcNSyaoY'
bot = TelegramBot.new(token: token)
bot.get_updates(fail_silently: true) do |message|
  puts "@#{message.from.username}: #{message.text}"
  command = message.get_command_for(bot)

  message.reply do |reply|
    case command
    when /start/i
      reply.text = "Add me to your group so we can start the process of telling people that you can't download their media."
    when /info/i
      reply.text = "This bot was made for the sole purpose of replying to people when they send any form of media to inform them that you cannot see and react to their message because your internet service is in Fair Consumption mode."
    else
      reply.text = "I don't get what you mean by #{command.inspect} ."
    end
    puts "Sending #{reply.text.inspect} to @#{message.from.username}"
    reply.send_with(bot)
  end
end
